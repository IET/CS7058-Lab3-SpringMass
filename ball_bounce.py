import numpy as np
import scipy
from scipy import signal
import pylab
from scipy.io import wavfile
import matplotlib.pyplot as plt
from matplotlib import animation

#Coefficient of Restitution
k = 0.1
equilibrium = 0.0
initial_position = 10.0
mass = 5
position = 0.0
velocity = 0.0
c = 0.5

# First set up the figure, the axis, and the plot element we want to animate
fig = plt.figure()
ax = plt.axes(xlim=(-2, 2), ylim=(-0.25	, 10))
line, = ax.plot([], [], lw=0, marker='.', markeredgewidth=5, markersize=20.0)

timetext = ax.text(50, 50, '')

#F = -kX
def undamped_spring_mass(position):
	force = -1 * k * (position - equilibrium)
	return force

#F = -kx + -cv
def damped_spring_mass():
	return (-1 * c * velocity) + (-1 * k * (position - equilibrium));

def apply_force(f):
	global position
	global velocity
	velocity += f/mass #consider implementing a time delta
	position += velocity
	return position

# initialization function: plot the background of each frame
def init():
	line.set_data([], [])
	return [line]

# animation function. This is called sequentially
def animate(i):
	global position
	apply_force(damped_spring_mass())
	#position = damped_spring_mass(i, 0, (2 * np.sqrt(k * mass)) - 0.9)
	line.set_data([0], [np.abs(position)])

	return tuple([line])


position = initial_position

# call the animator.  blit=True means only re-draw the parts that have changed.
anim = animation.FuncAnimation(fig, animate, init_func=init, frames=1000, interval=50, blit=True)

# save the animation as an mp4.  This requires ffmpeg or mencoder to be
# installed.  The extra_args ensure that the x264 codec is used, so that
# the video can be embedded in html5.  You may need to adjust this for
# your system: for more information, see
# http://matplotlib.sourceforge.net/api/animation_api.html

#print ("Saving animation as MPEG4")
#anim.save('ball_bounce.mp4', writer='ffmpeg', fps=30, extra_args=['-vcodec', 'libx264'])

plt.show()