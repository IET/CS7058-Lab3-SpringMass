import numpy as np
import scipy
from scipy import signal
import pylab
from scipy.io import wavfile
import matplotlib.pyplot as plt
from matplotlib import animation
import matplotlib.cm as cm

timestep = 0.1

#Coefficient of Restitution
k = 1.0
equilibrium = 0.0
initial_position = 5.0
mass = 5
position = 0.0

#F = -kx + -cv
def damped_spring_mass(c, t, phi = 0):
	global k
	global mass
	omega = np.sqrt(k/mass)
	zeta = c / (2 * np.sqrt(k * mass))
	return (initial_position * np.exp(-zeta * omega * t)) * np.cos((np.sqrt(1 - zeta**2)) * omega * t - phi)

def get_xt_plot(damping_coefficient, t):
	global mass
	position = initial_position
	points = []

	for i in t:		
		points.append([position])
		position = damped_spring_mass(damping_coefficient, i)
	return points

def plot_displacements(c_list, t):
	global k
	fig, axarr = plt.subplots(3,3)
	fig.set_size_inches(12, 9, forward=True)
	title = "Displacement vs. Time For Various c Values (k={0})".format(k)

	fig.suptitle(title, fontsize=20)

	axarr[0,0].set_title("c = " + str(c_list[0]))
	axarr[0,0].set_ylim(-5, 5)
	axarr[0,0].plot(t, get_xt_plot(c_list[0], t))
	
	axarr[0,1].set_title("c = " + str(c_list[1]))
	axarr[0,1].set_ylim(-5, 5)
	axarr[0,1].plot(t, get_xt_plot(c_list[1], t))

	axarr[0,2].set_title("c = " + str(c_list[2]))
	axarr[0,2].set_ylim(-5, 5)
	axarr[0,2].plot(t, get_xt_plot(c_list[2], t))
	
	axarr[1,0].set_title("c = " + str(c_list[3]))
	axarr[1,0].set_ylim(-5, 5)
	axarr[1,0].plot(t, get_xt_plot(c_list[3], t))

	axarr[1,1].set_title("c = " + str(c_list[4]))
	axarr[1,1].set_ylim(-5, 5)
	axarr[1,1].plot(t, get_xt_plot(c_list[4], t))

	axarr[1,2].set_title("c = " + str(c_list[5]))
	axarr[1,2].set_ylim(-5, 5)
	axarr[1,2].plot(t, get_xt_plot(c_list[5], t))

	axarr[2,0].set_title("c = " + str(c_list[6]))
	axarr[2,0].set_ylim(-5, 5)
	axarr[2,0].plot(t, get_xt_plot(c_list[6], t))

	axarr[2,1].set_title("c = " + str(c_list[7]))
	axarr[2,1].set_ylim(-5, 5)
	axarr[2,1].plot(t, get_xt_plot(c_list[7], t))

	axarr[2,2].set_title("c = " + str(c_list[8]))
	axarr[2,2].set_ylim(-5, 5)
	axarr[2,2].plot(t, get_xt_plot(c_list[8], t))

	plt.savefig("displacement_plots.png", dpi=96)
	plt.show()

t = []
time = 0
while (time < 120):
	t.append(time)
	time += timestep

plot_displacements([0.25, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0], t);