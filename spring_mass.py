import numpy as np
import scipy
from scipy import signal
import pylab
from scipy.io import wavfile
import matplotlib.pyplot as plt
from matplotlib import animation
import matplotlib.cm as cm

timestep = 0.01

#Coefficient of Restitution
k = 0.1
equilibrium = 0.0
initial_position = 10.0
mass = 5
position = 0.0

#F = -kx + -cv
def damped_spring_mass(position, velocity, c):
	return (-1 * c * velocity) + (-1 * k * (position - equilibrium));

def apply_force(f):
	velocity += f/mass #consider implementing a time delta
	position += velocity
	return position

def get_xt_plot(damping_coefficient, t):
	global mass
	position = initial_position
	velocity = 0
	points = []
	time = 0.0

	for i in t:		
		points.append([position])
		force = damped_spring_mass(position, velocity, damping_coefficient)
		velocity += force/mass
		position += velocity
		time += timestep
	return points

def plot_displacements(c_list, t):
	fig, axarr = plt.subplots(2,2)
	fig.set_size_inches(12, 9, forward=True)
	title = "Displacement vs. Time For Various c Values"

	fig.suptitle(title, fontsize=20)

	axarr[0,0].set_title("c = " + str(c_list[0]))
	axarr[0,0].set_ylim(-5, 5)
	axarr[0,0].plot(t, get_xt_plot(c_list[0], t))
	
	axarr[0,1].set_title("c = " + str(c_list[1]))
	axarr[0,1].set_ylim(-5, 5)
	axarr[0,1].plot(t, get_xt_plot(c_list[1], t))

	axarr[1,0].set_title("c = " + str(c_list[2]))
	axarr[1,0].set_ylim(-5, 5)
	axarr[1,0].plot(t, get_xt_plot(c_list[2], t))
	
	axarr[1,1].set_title("c = " + str(c_list[3]))
	axarr[1,1].set_ylim(-5, 5)
	axarr[1,1].plot(t, get_xt_plot(c_list[3], t))

	plt.savefig("displacement_plots.png", dpi=96)
	plt.show()

t = []
time = 0
while (time < 5):
	t.append(time)
	time += timestep

plot_displacements([0.25, 0.5, 1.0, 1.5], t);